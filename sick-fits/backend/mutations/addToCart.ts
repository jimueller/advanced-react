// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
// eslint-disable-next-line @typescript-eslint/restrict-template-expressions
import { KeystoneContext } from '@keystone-next/types';
import { CartItemCreateInput } from '../.keystone/schema-types';
import { Session } from '../types';

export default async function addToCart(
  root: any,
  { productId }: { productId: string },
  context: KeystoneContext
): Promise<CartItemCreateInput> {
  // 1 query current user to make sure they're signed in
  const session = context.session as Session;
  if (!session.itemId) {
    throw new Error('You must be logged in to add to your cart.');
  }

  // 2 query the user's cart
  const allCartItems = await context.lists.CartItem.findMany({
    where: { user: { id: session.itemId }, product: { id: productId } },
    resolveFields: 'id,quantity',
  });

  const [existingCartItem] = allCartItems;

  // 3 see if the item is already in the cart
  if (existingCartItem) {
    //   4a if so, increment
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return context.lists.CartItem.updateOne({
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      id: existingCartItem.id,
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access,@typescript-eslint/restrict-plus-operands
      data: { quantity: existingCartItem.quantity + 1 },
    });
  }

  //   4b if not, create new cart item
  return context.lists.CartItem.createOne({
    data: {
      product: { connect: { id: productId } },
      user: { connect: { id: session.itemId } },
    },
  });
}
