import { ListAccessArgs } from './types';
import { permissionsList } from './schemas/fields';

export function isSignedIn({ session }: ListAccessArgs): boolean {
  return !!session;
}

const generatedPermissions = Object.fromEntries(
  permissionsList.map((permission) => [
    permission,
    function ({ session }: ListAccessArgs): boolean {
      return !!session?.data.role?.[permission];
    },
  ])
);
export const permissions = {
  ...generatedPermissions,
};

// Rule based functions - can return a boolean or a filter
export const rules = {
  canManageProducts: ({ session }: ListAccessArgs) => {
    if (!isSignedIn({ session })) {
      return false;
    }
    // do they have permission of canManageProducts
    if (permissions.canManageProducts({ session })) {
      return true;
    }
    // if not, do they own the product
    return { user: { id: session?.itemId } };
  },
  canOrder: ({ session }: ListAccessArgs) => {
    if (!isSignedIn({ session })) {
      return false;
    }
    if (permissions.canManageCart({ session })) {
      return true;
    }
    return { user: { id: session?.itemId } };
  },
  canManageOrderItems: ({ session }: ListAccessArgs) => {
    if (!isSignedIn({ session })) {
      return false;
    }
    if (permissions.canManageCart({ session })) {
      return true;
    }
    return { order: { user: { id: session?.itemId } } };
  },
  canReadProducts: ({ session }: ListAccessArgs) => {
    if (permissions.canManageProducts({ session })) {
      return true;
    }
    return { status: 'AVAILABLE' };
  },
  canManageUsers: ({ session }: ListAccessArgs) => {
    if (!isSignedIn({ session })) {
      return false;
    }
    if (permissions.canManageUsers({ session })) {
      return true;
    }
    // otherwise only update themselves
    return { id: session?.itemId };
  },
};
