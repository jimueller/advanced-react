import { PAGINATION_QUERY } from '../components/Pagination';

export default function paginationField() {
  return {
    keyArgs: false, // tells apollo that we are taking care of caching ourselves
    read(existing = [], { args, cache }) {
      const { skip, first } = args;

      // read number of items on the page from the cache
      const data = cache.readQuery({ query: PAGINATION_QUERY });
      const count = data?.productsMeta?.count;
      const page = skip / first + 1;
      const pages = Math.ceil(count / first);

      // check if we have existing items (filter excludes any undefined values)
      const items = existing.slice(skip, skip + first).filter((x) => x);

      // Handle last page, and there is not a full page worth of items, send it anyway
      if (items.length && items.length !== first && page === pages) {
        return items;
      }

      if (items.length !== first) {
        // not enough items in cache, go to the network
        return false;
      }

      // if there are items, return from cache
      if (items.length) {
        return items;
      }

      // First thing it does is asks the read function for the items
      // We can either do one of two things
      // We can return the items b/c they are in the cache
      // Or, we can return false, which will trigger a network request to fetch items

      // go to the network if all else fails
      return false;
    },
    merge(existing, incoming, { args }) {
      // This runs when the client returns from the network with the products
      // and we can handle them in the cache
      const { skip, first } = args;
      // take copy of existing cache if they exist (exising will be null if user navs directly to a page, e.g. products/4)
      const merged = existing ? existing.slice(0) : [];
      // Merge in items to their position
      for (let i = skip; i < skip + incoming.length; ++i) {
        merged[i] = incoming[i - skip];
      }
      // return merged items from the cache
      return merged;
    },
  };
}
