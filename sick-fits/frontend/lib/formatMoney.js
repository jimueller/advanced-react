const usdFormatterCents = Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 2,
});

const usdFormatterNoCents = Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 0,
});

export default function formatMoney(cents = 0) {
  if (cents % 100 === 0) {
    return usdFormatterNoCents.format(cents / 100);
  }
  return usdFormatterCents.format(cents / 100);
}
