export default function calcTotalPrice(cart = []) {
  return cart.reduce((total, cartItem) => {
    if (!cartItem.product) {
      // Products could be deleted, but still in a cart
      return total;
    }
    return total + cartItem.product.price * cartItem.quantity;
  }, 0);
}
