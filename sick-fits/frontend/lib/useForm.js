import { useEffect, useState } from 'react';

export default function useForm(initial = {}) {
  const [inputs, setInputs] = useState(initial);
  const initialValues = Object.values(initial).join();

  useEffect(() => {
    // this func runs when the things we are watching change
    setInputs(initial);
  }, [initialValues]);

  function handleChange(e) {
    const { name, type } = e.target;
    let { value } = e.target;

    if (type === 'number') {
      value = parseInt(value);
    }

    if (type === 'file') {
      // eslint-disable-next-line prefer-destructuring
      value = e.target.files[0];
    }

    setInputs({ ...inputs, [name]: value });
  }

  function resetForm() {
    setInputs(initial);
  }

  function clearForm() {
    setInputs(
      Object.fromEntries(Object.entries(inputs).map(([prop]) => [prop, '']))
    );
  }

  return [inputs, handleChange, { resetForm, clearForm }];
}
