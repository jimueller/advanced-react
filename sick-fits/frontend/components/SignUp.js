import gql from 'graphql-tag';
import { useMutation } from '@apollo/client';
import Router from 'next/router';
import useForm from '../lib/useForm';
import Form from './styles/Form';
import { CURRENT_USER_QUERY } from '../lib/useUser';
import DisplayError from './ErrorMessage';

const SIGN_UP_MUTATION = gql`
  mutation SIGN_UP_MUTATION(
    $email: String!
    $name: String!
    $password: String!
  ) {
    createUser(data: { name: $name, email: $email, password: $password }) {
      id
      email
    }
  }
`;

export default function SignUp() {
  const [inputs, handleChange, { resetForm }] = useForm({
    email: '',
    name: '',
    password: '',
  });

  // an unsuccessful signing is not an "error", the deets will be in the data
  const [signup, { data, error, loading }] = useMutation(SIGN_UP_MUTATION, {
    variables: inputs,
  });

  const handleSubmit = async (e) => {
    e.preventDefault();
    await signup().catch(console.error);
    resetForm();
  };

  console.log(error);

  return (
    <Form
      // POST method to prevent password from ending up in URL
      method="POST"
      onSubmit={handleSubmit}
    >
      <h2>Sign Up For An Account</h2>
      <DisplayError error={error} />
      {data?.createUser ? (
        <p>
          Signed up with {data.createUser.email} - Please go ahead and sign in.
        </p>
      ) : (
        <fieldset aria-busy={loading}>
          <label htmlFor="name">
            Name
            <input
              type="text"
              id="name"
              name="name"
              onChange={handleChange}
              value={inputs.name}
              autoComplete="name"
            />
          </label>
          <label htmlFor="email">
            Email
            <input
              type="text"
              id="email"
              name="email"
              onChange={handleChange}
              value={inputs.email}
              autoComplete="email"
            />
          </label>
          <label htmlFor="password">
            Password
            <input
              type="password"
              id="password"
              name="password"
              value={inputs.password}
              onChange={handleChange}
            />
          </label>
          <button type="submit">Sign Up</button>
        </fieldset>
      )}
    </Form>
  );
}
