import { useMutation } from '@apollo/client';
import Router from 'next/router';
import gql from 'graphql-tag';
import { CURRENT_USER_QUERY } from '../lib/useUser';

const SIGN_OUT_MUTATION = gql`
  mutation {
    endSession
  }
`;
export default function SignOut() {
  const [endSession] = useMutation(SIGN_OUT_MUTATION, {
    refetchQueries: [
      {
        query: CURRENT_USER_QUERY,
      },
    ],
  });

  const handleSignOut = async () => {
    await endSession();
    await Router.push('/');
  };

  return (
    <button type="button" onClick={handleSignOut}>
      Sign Out
    </button>
  );
}
