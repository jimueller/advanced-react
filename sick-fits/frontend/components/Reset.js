import gql from 'graphql-tag';
import { useMutation } from '@apollo/client';
import Router from 'next/router';
import useForm from '../lib/useForm';
import Form from './styles/Form';
import { CURRENT_USER_QUERY } from '../lib/useUser';
import DisplayError from './ErrorMessage';

const RESET_MUTATION = gql`
  mutation RESET_MUTATION(
    $email: String!
    $token: String!
    $password: String!
  ) {
    redeemUserPasswordResetToken(
      email: $email
      token: $token
      password: $password
    ) {
      message
      code
    }
  }
`;

export default function Reset({ token }) {
  const [inputs, handleChange, { resetForm }] = useForm({
    email: '',
    token,
    password: '',
  });

  // an unsuccessful signing is not an "error", the deets will be in the data
  const [resetPassword, { data, error, loading }] = useMutation(
    RESET_MUTATION,
    {
      variables: inputs,
    }
  );

  const handleSubmit = async (e) => {
    e.preventDefault();
    await resetPassword().catch(console.error);
    resetForm();
  };

  const resetError = data?.redeemUserPasswordResetToken?.code
    ? data.redeemUserPasswordResetToken
    : undefined;

  return (
    <Form
      // POST method to prevent password from ending up in URL
      method="POST"
      onSubmit={handleSubmit}
    >
      <h2>Reset your password</h2>
      <DisplayError error={error || resetError} />
      {data?.redeemUserPasswordResetToken === null ? (
        <p>Success! Sign in</p>
      ) : (
        <fieldset aria-busy={loading}>
          <label htmlFor="email">
            Email
            <input
              type="text"
              id="email"
              name="email"
              onChange={handleChange}
              value={inputs.email}
              autoComplete="email"
            />
          </label>
          <label htmlFor="password">
            Password
            <input
              type="password"
              id="password"
              name="password"
              value={inputs.password}
              onChange={handleChange}
            />
          </label>
          <button type="submit">Reset Password</button>
        </fieldset>
      )}
    </Form>
  );
}
