import gql from 'graphql-tag';
import { useMutation } from '@apollo/client';
import Router from 'next/router';
import useForm from '../lib/useForm';
import Form from './styles/Form';
import { CURRENT_USER_QUERY } from '../lib/useUser';
import DisplayError from './ErrorMessage';

const SIGN_IN_MUTATION = gql`
  mutation SIGN_IN_MUTATION($email: String!, $password: String!) {
    authenticateUserWithPassword(email: $email, password: $password) {
      ... on UserAuthenticationWithPasswordSuccess {
        item {
          id
          name
        }
      }
      ... on UserAuthenticationWithPasswordFailure {
        code
        message
      }
    }
  }
`;

export default function SignIn() {
  const [inputs, handleChange, { resetForm }] = useForm({
    email: '',
    password: '',
  });

  // an unsuccessful signing is not an "error", the deets will be in the data
  const [signin, { data, error, loading }] = useMutation(SIGN_IN_MUTATION, {
    variables: inputs,
    // Refecth the currently logged in user
    refetchQueries: [
      {
        query: CURRENT_USER_QUERY,
      },
    ],
  });

  const loginError =
    data?.authenticateUserWithPassword?.__typename ===
    'UserAuthenticationWithPasswordFailure'
      ? data.authenticateUserWithPassword
      : undefined;

  const handleSubmit = async (e) => {
    e.preventDefault();
    await signin();
    resetForm();
    // TODO check res and redirect on success
    // await Router.push('/');
  };

  return (
    <Form
      // POST method to prevent password from ending up in URL
      method="POST"
      onSubmit={handleSubmit}
    >
      <h2>Sign Into Your Account</h2>
      <DisplayError error={error || loginError} />
      <fieldset>
        <label htmlFor="email">
          Email
          <input
            type="text"
            id="email"
            name="email"
            onChange={handleChange}
            value={inputs.email}
            autoComplete="email"
          />
        </label>
        <label htmlFor="password">
          Password
          <input
            type="password"
            id="password"
            name="password"
            value={inputs.password}
            onChange={handleChange}
          />
        </label>
        <button type="submit">Sign In</button>
      </fieldset>
    </Form>
  );
}
