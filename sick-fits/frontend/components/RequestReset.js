import gql from 'graphql-tag';
import { useMutation } from '@apollo/client';
import Router from 'next/router';
import useForm from '../lib/useForm';
import Form from './styles/Form';
import { CURRENT_USER_QUERY } from '../lib/useUser';
import DisplayError from './ErrorMessage';

const REQUEST_RESET_MUTATION = gql`
  mutation REQUEST_RESET_MUTATION($email: String!) {
    sendUserPasswordResetLink(email: $email) {
      message
      code
    }
  }
`;

export default function RequestReset() {
  const [inputs, handleChange, { resetForm }] = useForm({
    email: '',
    name: '',
    password: '',
  });

  // an unsuccessful signing is not an "error", the deets will be in the data
  const [signup, { data, error, loading }] = useMutation(
    REQUEST_RESET_MUTATION,
    {
      variables: inputs,
    }
  );

  const handleSubmit = async (e) => {
    e.preventDefault();
    await signup().catch(console.error);
    resetForm();
  };

  console.log(error);

  return (
    <Form
      // POST method to prevent password from ending up in URL
      method="POST"
      onSubmit={handleSubmit}
    >
      <h2>Request a Password Reset</h2>
      <DisplayError error={error} />
      {data?.sendUserPasswordResetLink === null ? (
        <p>Success! Check your email for a link to reset your password.</p>
      ) : (
        <fieldset aria-busy={loading}>
          <label htmlFor="email">
            Email
            <input
              type="text"
              id="email"
              name="email"
              onChange={handleChange}
              value={inputs.email}
              autoComplete="email"
            />
          </label>
          <button type="submit">Request Reset</button>
        </fieldset>
      )}
    </Form>
  );
}
