import gql from 'graphql-tag';
import { useMutation } from '@apollo/client';
import { CURRENT_USER_QUERY } from '../lib/useUser';
import { useCart } from '../lib/cartState';

const ADD_TO_CART_MUTATION = gql`
  mutation ADD_TO_CART_MUTATION($id: ID!) {
    addToCart(productId: $id) {
      id
    }
  }
`;
export default function AddToCart({ id }) {
  const { openCart } = useCart();
  const [addToCart, { loading }] = useMutation(ADD_TO_CART_MUTATION, {
    variables: {
      id,
    },
    refetchQueries: [{ query: CURRENT_USER_QUERY }],
  });

  const handleClick = async () => {
    await addToCart();
    openCart();
  };

  return (
    <button
      type="button"
      aria-busy={loading}
      disabled={loading}
      onClick={handleClick}
    >
      Add{loading && 'ing'} To Cart 🛒
    </button>
  );
}
